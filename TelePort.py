#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    Copyright (C) 2016 Mikael Holber http://www.beam-project.com/teleport
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#    or download it from http://www.gnu.org/licenses/gpl.txt
#
#
#
# This Python file uses the following encoding: utf-8

import wx
import platform
import os
import sys

from bin.appsettings import *
from bin.dialogs.teleportmainframe import teleportMainFrame

app = wx.App(False) # Error messages go to terminal

########################################################
# Load Settings (global object)
########################################################
appSettings.LoadConfig(appSettings.defaultConfigFileName)

print (appSettings.mainFrameTitle)

########################################################
# Select logging method (terminal or file)
########################################################
if appSettings._logging == 'True':
    sys.stdout = open(appSettings._logPath,"w")

########################################################
# Start the main window
########################################################
top = teleportMainFrame()   # Creates the main frame
top.Show()                  # Shows the main frame

########################################################
# Start the main loop
########################################################

app.MainLoop()              # Start the main loop which handles events
