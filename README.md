# TELEPORT #

Welcome to TelePort-applications repository. These are the instructions how to run and build the source code on Windows, Mac, and Linux.

### Summary ###

* Quick Summary
* Requirements
* Running TelePort
* Packaging TelePort
* Contact

### Quick Summary ###

TelePort is an application to export playlists and songs into new directories. The application support sorting, managing and renaming song. TelePort use [Mutagen](http://mutagen.readthedocs.org/en/latest/) to read tags in the files. 

### Requirements ###

* Python 2.7.8 or later
* WxPython 3.0 or later

### Running TelePort ###

Download the repository, navigate to folder using terminal. Run 'python TelePort.py'

### Building TelePort ###

No current information

### Contact ###

* Official website: www.beam-project.com/teleport
* Repo owner or admin