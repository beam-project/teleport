#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    Copyright (C) 2016 Mikael Holber http://http://www.beam-project.com/teleport
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#    or download it from http://www.gnu.org/licenses/gpl.txt
#
#
# This Python file uses the following encoding: utf-8

import json, wx, platform
import io, os, sys

###############################################################
#
# AppSettings
#
###############################################################

class AppSettings:
    
    # strings resources JSON format
    filename = os.path.join(os.getcwd(), 'resources', 'text', 'strings.txt')
    stringResources = json.load(open(filename, "r"))
        
    defaultConfigFileName = stringResources["defaultConfigFileName"]
    defaultLogFileName = stringResources["defaultLogFileName"]
    mainFrameTitle = stringResources["mainFrameTitle"]
    aboutDialogDescription = stringResources["aboutDialogDescription"]
    aboutDialogLicense = stringResources["aboutDialogLicense"]
    aboutCopyright = stringResources["aboutCopyright"]
    aboutWebsite = stringResources["aboutWebsite"]
    aboutDeveloper = stringResources["aboutDeveloper"]
    appVersion = stringResources["version"]
    
###############################################################
#
# Init
#
###############################################################
    def __init__(self):
        self._configVersion = ''
        self._appName = ''
        self._windowSize = (500,500)
        self._logging = 'false'
        self._logPath = ''
    

###############################################################
#
# LOAD Config
#
###############################################################
    def LoadConfig(self, inputConfigFile):
        try:
            #Try loading in home directory
            ConfigData = self.OpenSetting(os.path.join(os.path.expanduser("~"), inputConfigFile))
            # Validate config
            if not ConfigData[u'ConfigVersion'] == self._appVersion:
                raise Exception('Config Version error, loading default')
        except:
            #Use original config
            ConfigData = self.OpenSetting(os.path.join(os.getcwd(), inputConfigFile))
        # Also load the original settingsfile
        ConfigDataOriginal = self.OpenSetting(os.path.join(os.getcwd(), inputConfigFile))
        self.ReadConfig(ConfigData, ConfigDataOriginal)
        return


    def ReadConfig(self, ConfigData, ConfigDataOriginal):
        self._configVersion     = self.ExtractSetting(ConfigData,ConfigDataOriginal,u'ConfigVersion')
        self._appName           = self.ExtractSetting(ConfigData,ConfigDataOriginal,u'AppName')
        self._logging           = self.ExtractSetting(ConfigData,ConfigDataOriginal,u'AppLogging')
        self._logPath           = self.ExtractSetting(ConfigData,ConfigDataOriginal,u'LogPath')
        if self._logPath == '':
            self._logPath = os.path.join(os.path.expanduser("~"), AppSettings.defaultLogFileName)

        # Set OS-specific variables
        if platform.system() == 'Linux':
            self._windowSize = (500, 500)
        if platform.system() == 'Windows':
            self._windowSize = (500, 500)
        if platform.system() == 'Darwin':
            self._dinowSize = (400, 600)

        return
#
# SUPPORTING FUNCTIONS
#
    def ExtractSetting(self, ConfigData, ConfigDataOriginal, key):
        try:
            output = ConfigData[key]
        except:
            output = ConfigDataOriginal[key]
        return output

    def OpenSetting(self, inputConfigFile):
        ConfigFile = open(inputConfigFile, 'r')
        ConfigData = json.load(ConfigFile)
        ConfigFile.close()
        return ConfigData


###############################################################
#
# Save Config
#
###############################################################
    def SaveConfig(self, outputConfigFile):

        output = {}

        output[u'Configname']       = "Default Configuration"
        output[u'Comment']          = "This is a configuration file for TelePort"
        output[u'Author']           = "Mikael Holber & Horia Uifaleanu - 2016"
        output[u'AppName']          = self._appName
        output[u'ConfigVersion']    = self._configVersion
        output[u'AppLogging']       = self._logging
        


        # Write config file to home dir
        self.WriteSetting(os.path.join(os.path.expanduser("~"),outputConfigFile), output)

        return


    def WriteSetting(self, outputConfigFile, output):
        ConfigFile = open(outputConfigFile, 'w')
        # Writing different format depending on platform
        if platform.system() == 'Windows':
            json.dump(output, ConfigFile, indent=2, encoding="latin-1")
        else:
            json.dump(output, ConfigFile, indent=2, encoding="utf-8")
        ConfigFile.close()
        return 

###############################################################
#
# Create object
#
###############################################################

appSettings = AppSettings()
