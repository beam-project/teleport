#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    Copyright (C) 2014 Mikael Holber http://http://www.beam-project.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#    or download it from http://www.gnu.org/licenses/gpl.txt
#
#
#    Revision History:
#
#    XX/XX/2014 Version 1.0
#       - Initial release
#
# This Python file uses the following encoding: utf-8


import wx, wx.html
import os, sys
import wx.lib.delayedresult

from bin.appsettings import *
from bin.dialogs import aboutdialog
from bin.dialogs import closedialog

from copy import deepcopy

##################################################
# MAIN WINDOW - FRAME
##################################################
class teleportMainFrame(wx.Frame):
    def __init__(self, settings = None):
    # Size and position of the main window
        wx.Frame.__init__(self, None, title=appSettings.mainFrameTitle, pos=(150,150), size=(800,600), style=wx.DEFAULT_FRAME_STYLE & ~ (wx.RESIZE_BORDER | wx.RESIZE_BOX | wx.MAXIMIZE_BOX))
        panel = wx.Panel(self)
    
    # Create Sizers
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        vbox = wx.BoxSizer(wx.VERTICAL)
        
    # Set Icon
        iconFilename = os.path.join(os.getcwd(),'resources','icons','app-icon','icon_Teleport_256px.png')
        self.favicon = wx.Icon(iconFilename, wx.BITMAP_TYPE_ANY, 256, 256)
        self.SetIcon(self.favicon)

    # Setting up the menu.
        self.filemenu    = wx.Menu()
        self.Aboutmenu   = wx.Menu()
        self.menuExit    = self.filemenu.Append(wx.ID_EXIT,"E&xit"," Terminate the program")
        self.menuAbout   = self.Aboutmenu.Append(wx.ID_ABOUT, "&About"," Information about this program")

    # Creating the menubar.
        self.menuBar = wx.MenuBar()
        self.menuBar.Append(self.filemenu,"&File")    # Adding the "file menu" to the MenuBar
        self.menuBar.Append(self.Aboutmenu,"&About")  # Adding the "About menu" to the MenuBar
        self.SetMenuBar(self.menuBar)                 # Adding the MenuBar to the Frame content.
        
    # Events.
        self.Bind(wx.EVT_MENU, self.OnClose, self.menuExit)
        self.Bind(wx.EVT_MENU, self.OnAbout, self.menuAbout)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
    
    # BUILDING THE GUI
        box1 = wx.StaticBox(panel, label="1 - Input directory")
        box2 = wx.StaticBox(panel, label="2 - Output directory")
        box3 = wx.StaticBox(panel, label="3 - Options")
        
        
    # BUTTONS
        analyseButton = wx.Button(panel, label="Analyse")
        executeButton = wx.Button(panel, label="Execute")
        analyseButton.Bind(wx.EVT_BUTTON, self.OnAbout)
        executeButton.Bind(wx.EVT_BUTTON, self.OnAbout)
        hbox.Add(analyseButton, flag= wx.LEFT | wx.TOP, border=10)
        hbox.Add(executeButton, flag= wx.ALL, border=10)

        
    # SIZERS
        vbox.Add(box1, flag=wx.ALL |wx.EXPAND, border=5)
        vbox.Add(box2, flag=wx.ALL |wx.EXPAND, border=5)
        vbox.Add(box3, flag=wx.ALL |wx.EXPAND, border=5)
        vbox.Add(hbox, flag=wx.ALIGN_RIGHT |wx.EXPAND)
        panel.SetSizer(vbox)
        self.Layout()
        
    # STATUSBAR - MUST BE CREATED AFTER LAYOUT
        self.statusbar = self.CreateStatusBar()
        self.SetStatusText('Ready...')


########################## END FRAME INITIALIZATION #########################


########################################################
# Buttons and menues
########################################################

    #
    # Show 'Close dialog
    #
    def OnClose(self, event):
        closedialog.ShowCloseDialog(self)
    #
    # Show 'About Dialog'
    #
    def OnAbout(self, event):
        aboutdialog.ShowAboutDialog(self)
    
    
    
    
    
    
    
########################################################
#                                                      #
#                                                      #
#                 DATA FUNCTIONS                       #
#                                                      #
#                                                      #
########################################################

